$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $("#contactoBtn").on('hide.bs.modal', function(){
      console.log("Modal se muestra");
      $(this).html("Visto");
    });

    $("#contactoBtn").on('click', function(){
      console.log("Modal se muestra");
      if($(this).html() == "Visto"){
        $(this).html("Contactar");
        $(this).addClass("btn-outline-succes");
        $(this).removeClass("btn-outline-dark");
      }else{
        $(this).html("Visto");
        $(this).removeClass("btn-outline-succes");
        $(this).addClass("btn-outline-dark");
      }
    });


  });
  